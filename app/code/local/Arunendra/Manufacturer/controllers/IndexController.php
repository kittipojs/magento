<?php
/**
 * Created by PhpStorm.
 * User: Kittipoj
 * Date: 10/9/2017
 * Time: 4:32 PM
 */
class Arunendra_Manufacturer_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("CustomPage"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("CustomMenu1", array(
            "label" => $this->__("CustomMenu1"),
            "title" => $this->__("CustomMenu1"),
            "link"  => "manufacturer"
        ));

        $breadcrumbs->addCrumb("CustomMenu2", array(
            "label" => $this->__("CustomMenu2"),
            "title" => $this->__("CustomMenu2"),
            "link"  => "custom"
        ));

        $this->renderLayout();

    }
}