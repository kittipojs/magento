<?php
/**
 * Created by PhpStorm.
 * User: Kittipoj
 * Date: 10/9/2017
 * Time: 4:32 PM
 */
class Arunendra_Manufacturer_Block_Index extends Mage_Core_Block_Template{
    public function index()
    {

        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'manufacturer');
        return $attribute;
    }

}