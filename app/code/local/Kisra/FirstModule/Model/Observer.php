<?php
/**
 * Created by PhpStorm.
 * User: Kittipoj
 * Date: 10/19/2017
 * Time: 1:47 PM
 */

class Kisra_FirstModule_Model_Observer
{

    public function addtocartEvent(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();  //Gets the event
        $product = $event->getProduct();
        $observermsg = "The event triggered>>> <B>" . $event->getName() . "</B><br/> The added product>>> <B> " . $product->getName() . "</B>";
        $rulesCollection = Mage::getModel('salesrule/rule')->getCollection();
        //Adds the message to the shopping cart
        foreach ($rulesCollection as $rule) {
            $rule_name = $rule->getName();
            $rule_id = $rule->getid();
            Mage::getSingleton('core/session')->addNotice($rule_name . ' :: id = ' . $rule_id);

            $con = unserialize($rule->getConditionsSerialized());
            foreach ($con as $q) {
                Mage::getSingleton('core/session')->addSuccess($q);
            }
        }
        Mage::getSingleton('core/session')->addWarning($observermsg);
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $ruleIds = $quote->getAppliedRuleIds();
        Mage::getSingleton('core/session')->addWarning($ruleIds);
    }

    public function autoApproveReview(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $reviewObjData = $event->getData();
        //print_r($reviewObjData);exit;

        $reviewData = $reviewObjData["data_object"];
        $reviewData->setStatusId(Mage_Review_Model_Review::STATUS_APPROVED);

        echo Mage::getSingleton('core/session')->addSuccess("Thank you for your input!!");
    }
}